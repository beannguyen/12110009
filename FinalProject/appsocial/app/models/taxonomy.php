<?php namespace models;

use \helpers\pagenavigation;

class Taxonomy extends \core\model
{
	private $_mApp;

    function __construct()
    {
        parent::__construct();
        $this->_mApp = new \models\apps();
    }

    function getTaxonomyInfo( $slug )
    {
    	// get taxonomy id
    	$sql = "SELECT * FROM ". PREFIX ."taxonomy WHERE slug = '". $slug ."'";
        $query = $this->_db->query( $sql );

        return $this->_db->fetch( $query );
    }

    function getTaxonomyIdBySlug( $slug )
    {
    	$sql = "SELECT id FROM ". PREFIX ."taxonomy WHERE slug = '". $slug ."'";
        $query = $this->_db->query( $sql );

        return $this->_db->fetch( $query )['id'];
    }

    function getTaxonomyApps( $id )
    {
    	$sql = "SELECT * FROM ". PREFIX ."taxonomy_relationships WHERE term_id = " . $id;

    	if ( isset( $_GET['page'] ) )
			$page = $_GET['page'];
		else
			$page = 1;

		$navigation = new PageNavigation( $sql, 10, 10, \helpers\url::curURL(), $page );

		$newSql = $navigation->paginate();

    	$query = $this->_db->query( $newSql );

    	$listId = array();
    	while ( $row = $this->_db->fetch( $query ) ) {
    		
    		$listId[] = $row['object_id'];
    	}

    	$listId['navigation'] = $navigation->renderFullNav();

    	return $listId;
    }
}
<?php namespace models;

class Ajax extends \core\model
{
    function __construct()
    {
        parent::__construct();
        $this->_db->connect();
    }

    /**
     * Insert/Update url formats
     * @param string $action
     * @param $data
     * @param $linkId
     * @return bool
     */
    function url_format( $action = 'insert', $data, $linkId = false )
    {
        $table = PREFIX . 'search_url_formed';

        if ( $action === 'insert' ) {

            if ( $this->_db->insert( $table, $data ) )
                return true;
        } else {

            $where = ' id = ' . $linkId;
            if ( $this->_db->update( $table, $data, $where ) )
                return true;
        }

        return false;
    }

    /**
     * get all field url format
     * @param $linkId
     * @return array
     */
    function get_url_format( $linkId )
    {
        $sql = "SELECT * FROM ". PREFIX ."search_url_formed WHERE id = " . $linkId;
        $query = $this->_db->query( $sql );
        $result = $this->_db->fetch( $query );

        return $result;
    }

    function delete_url_format( $linkId )
    {
        $sql = "DELETE FROM ". PREFIX ."search_url_formed WHERE id = " . $linkId;
        if ( $this->_db->query( $sql ) )
            return 'deleted';
        else
            return 'error';
    }
}
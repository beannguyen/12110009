<?php namespace models\backend;

class Register extends \core\model
{
    function __construct()
    {
        parent::__construct();
        $this->_db->connect();

        $this->use_emails = $this->generic->getOption('email-as-username-enable');
        $this->username_type = ($this->use_emails) ? 'email' : 'username';
    }

    function isExistedEmail($email)
    {
        $sth = $this->_db->query("SELECT email FROM " . PREFIX . "users WHERE
                    email = '$email'");

        $count = $this->_db->numrows($sth);

        if ($count > 0) {
            return 1; // username already existed
        } else
            return 0;
    }
}
<?php namespace models\backend;

use helpers\Session,
    helpers\password;

class Login extends \core\model {

    // Post vars
    private $user;
    private $pass;

    public $use_emails = false;

    // Misc vars
    private $token;
    private $result;
    private $error;
    private $msg;

    function __construct() {

        parent::__construct();

        // Get user login type
        $this->use_emails = $this->generic->getOption('email-as-username-enable');
        $this->username_type = ( $this->use_emails ) ? 'email' : 'username';
    }

    public function process($user, $pass, $token) {

        $this->user = $user;
        $this->pass = $pass;
        $this->token = $token;

        // Check that the token is valid, prevents exploits
        if( !$this->generic->valid_token( $this->token ) ) {
            return 'invalid_token';
        }


        // Confirm all details are correct and fetch result
        $valid = $this->validate();
        if($valid == 1)
        {
            // Log the user in
            $result = $this->login();
            return $result;
        } else
        {
            return $valid;
        }

    }

    private function validate() {

        if(!empty($this->error)) return 'error_found';

        $username = $this->username_type;
        $stmt = $this->_db->query("SELECT * FROM ".PREFIX."users WHERE {$username} = '$this->user'");
        if( $this->_db->numrows($stmt) == 1 )
        {
            $this->result = $this->_db->fetch( $stmt );

            if( $this->generic->getOption( 'disable-logins-enable' ) == '1' && $this->result['user_id'] !== '1' ) {

                return 'disable-login';
            } elseif ( !$this->isActivated() ) {

                return 'not_active';
            } elseif ( $this->result['restricted'] == 1 || ( $this->isDisabledLevel( unserialize( $this->result['user_level'] )[0] ) ) ) {

                return 'banned_user';
            } elseif ( !Password::verify( $this->pass, $this->result['password'] ) ) {

                return 'incorrect_password';
            }
        } else
            return 'incorrect_password';

        return 1;
    }

    /**
     * Check user is disabled or not
     * @param $level
     * @return bool
     */
    private function isDisabledLevel( $level ) {

        $sql = "SELECT level_disabled FROM " . PREFIX . "user_level WHERE id = " . $level;
        $query = $this->_db->query( $sql );
        $result = $this->_db->fetch( $query );
        if( $result['level_disabled'] == 1 )
            return true;
        return false;
    }

    /**
     * Verifies if the user's account is activated.
     *
     * If the account is not activated, we redirect them to the
     * userActivate.php page where further instruction is given.
     */
    private function isActivated() {

        /* See if the admin requires new users to activate */
        if ( $this->generic->getOption( 'user-activation-enable' ) ) {

            /** Check if user still requires activation. */
            $username = $this->username_type;

            $sql = "SELECT * FROM `".PREFIX."login_confirm` WHERE `{$username}` = '$this->user' AND `type` = 'new_user'";
            $query = $this->_db->query( $sql );
            $count = $this->_db->numrows( $query );

            if ($count > 0)
                return false;

        }
        return true;
    }

    // Once everything's filled out
    public function login() {

        // Just double check there are no errors first
        if( !empty($this->error) ) return 'error_found';

        // Session expiration
        $minutes = $this->generic->getOption('default_session');
        ini_set('session.cookie_lifetime', 60 * $minutes);

        session_regenerate_id();

        // Save if user is restricted
        if ( !empty($this->result['restricted']) ) Session::set( 'restricted', 1 );

        // Save user's current level
        $user_level = unserialize($this->result['user_level']);
        Session::set( 'user_level', $user_level );

        Session::set( 'email', $this->result['email'] );

        /** Check whether the user's level is disabled. */
        $query = $this->_db->query("SELECT `level_disabled` FROM `". PREFIX ."user_level` WHERE `id` = $user_level[0];");
        $disRow = $this->_db->fetch( $query );

        if ( !empty($disRow['level_disabled']) ) Session::set( 'level_disabled', 1 );

        // Stay signed via checkbox?
        if(isset($_POST['remember'])) {
            ini_set('session.cookie_lifetime', 60*60*24*100); // Set to expire in 3 months & 10 days
            session_regenerate_id();
        }

        /** Store a timestamp. */
        if( $this->generic->getOption('profile-timestamps-enable') ) {

            $table = PREFIX . "login_timestamps";
            $data = array(
                'user_id' => $this->result['user_id'],
                'ip' => $this->generic->getIPAddress(),
                'timestamp' => 'CURRENT_TIMESTAMP'
            );
            $this->_db->insert( $table, $data );

        }

        // And our magic happens here ! Let's sign them in
        $username = $this->username_type;
        Session::set( 'username', $this->result[$username] );

        Session::set( 'fullname', $this->result['name'] );

        // User ID of the logging in user
        Session::set( 'user_id', $this->result['user_id'] );

        $redirect = Session::get( 'referer' );

        unset(
        $_SESSION['ssbidbuy']['referer'],
        $_SESSION['ssbidbuy']['token'],
        $_SESSION['ssbidbuy']['facebookMisc'],
        $_SESSION['ssbidbuy']['twitterMisc'],
        $_SESSION['ssbidbuy']['openIDMisc']
        );

        // Redirect after it's all said and done
        return $redirect;

    }
}
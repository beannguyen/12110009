<?php namespace models\backend;

class User extends \core\model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Return userId by email
     * @param $email
     * @return int userId
     */
    function getUserIdByEmail( $email )
    {

        $sql = "SELECT user_id FROM " . PREFIX . "users WHERE email = '" . $email . "'";
        $query = $this->_db->query($sql);
        $result = $this->_db->fetch($query);

        return $result['user_id'];
    }

    /**
     * @param $userId
     * @param $password
     * @return bool
     */
    function changePassword($userId, $password)
    {
        $sql = "UPDATE " . PREFIX . "users SET password = '" . $password . "' WHERE user_id = " . $userId;
        $query = $this->_db->query($sql);
        if ($query)
            return true;
        return false;
    }

    /**
     * @param $userId
     * @return bool
     */
    function getUser($userId)
    {
        $sql = "SELECT * FROM " . PREFIX . "users WHERE user_id = " . $userId;
        $q = $this->_db->query($sql);

        if ($this->_db->numrows($q) == 0)
            return false;

        $result = $this->_db->fetch($q);
        return $result;
    }
}
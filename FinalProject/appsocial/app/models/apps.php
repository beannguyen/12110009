<?php namespace models;

use \helpers\pagenavigation;

class Apps extends \core\model
{

	function __construct()
	{
		parent::__construct();
	}

	function getAllApps()
	{
		$apps = array();

		$sql = "SELECT * FROM " . PREFIX . "apps_indexed";

		if ( isset( $_GET['page'] ) )
			$page = $_GET['page'];
		else
			$page = 1;
		$navigation = new PageNavigation( $sql, 10, 10, \helpers\url::curURL(), $page );
		$newSql = $navigation->paginate();

        $query = $this->_db->query( $newSql );

        while ( $row = $this->_db->fetch( $query ) ) {

        	$apps[$row['id']] = array();
        	foreach ( $row as $k => $v ) {

        		$apps[$row['id']][$k] = $v;
        	}

        	$sql = "SELECT meta_key, meta_value FROM ". PREFIX ."apps_indexed, ". PREFIX ."app_metas 
	                WHERE ". PREFIX ."apps_indexed.id = ". PREFIX ."app_metas.app_id
	                AND ". PREFIX ."apps_indexed.id = " . $row['id'];
	        $q = $this->_db->query( $sql );

	        while( $result = $this->_db->fetch( $q ) ) {

	        	$apps[$row['id']][$result['meta_key']] = $result['meta_value'];
	        }
        }

        $apps['navigation'] = $navigation->renderFullNav();

        return $apps;
	}

	function getAppMetas( $id )
	{
		$sql = "SELECT meta_key, meta_value FROM ". PREFIX ."apps_indexed, ". PREFIX ."app_metas 
	                WHERE ". PREFIX ."apps_indexed.id = ". PREFIX ."app_metas.app_id
	                AND ". PREFIX ."apps_indexed.id = " . $id;
	    $q = $this->_db->query( $sql );

	    while( $result = $this->_db->fetch( $q ) ) {

	      	$app[$result['meta_key']] = $result['meta_value'];
	    }

	    return $app;
	}

	function getSearchResults( $key )
	{
		$list = array();

		$sql = "select id
				from ". PREFIX ."apps_indexed
				where app_title LIKE '%". $key ."%'
				OR app_description LIKE '%". $key ."%'
				OR app_author_name LIKE '%". $key ."%'";

		if ( isset( $_GET['page'] ) )
			$page = $_GET['page'];
		else
			$page = 1;
		$navigation = new PageNavigation( $sql, 10, 10, \helpers\url::curURL(), $page );
		$newSql = $navigation->paginate();

		$query = $this->_db->query( $newSql );

		while ( $row = $this->_db->fetch( $query ) ) {
			
			$list[] = $row['id'];
		}

		$list['navigation'] = $navigation->renderFullNav();

		return $list;
	}
}
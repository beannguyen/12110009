<?php namespace models;

class Bots extends \core\model
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     *  get link format to index
     *  @param string $type
     *  @return array $links array of links
     */
    function getLinks( $type )
    {
        if ( $type === 'collection') {


            $sql = "SELECT * FROM ". PREFIX ."search_url_formed WHERE type = '". $type ."'";
            $query = $this->_db->query( $sql );
            $result = $this->_db->fetch( $query );

            $attribute = $result['link_attribute'];
            $attribute = explode( ',', $attribute );

            $links = array();
            foreach ( $attribute as $k => $v ) {

                $links[$v] = $result['link_format'] . $v;
            }
        } else {

            $sql = "SELECT * FROM ". PREFIX ."taxonomy WHERE type = '". $type ."'";
            $query = $this->_db->query( $sql );
            
            $links = array();
            while ( $result = $this->_db->fetch( $query) ) {
                
                $links[] = 'https://play.google.com/store/apps/category/' . $result['slug'] . '/collection/topselling_paid';
                $links[] = 'https://play.google.com/store/apps/category/' . $result['slug'] . '/collection/topselling_free';
            }
        }

        return $links;
    }

    /**
     *  is package indexed?
     *  @param string name of package
     *  @return bool
     */
    function isIndexed( $package )
    {
        $sql = "SELECT * FROM ". PREFIX ."apps_indexed WHERE app_id = '". $package ."'";
        $query = $this->_db->query( $sql );

        if ( $this->_db->numrows( $query ) > 0 )
            return true;
        return false;
    }

    function insertAppInfo( $appInfo )
    {
        var_dump( $appInfo );

        // insert apps basic information
        $table = PREFIX . 'apps_indexed';
        $data = array(
                'app_id' => $appInfo['package'],
                'app_title' => $appInfo['title'],
                'app_description' => $appInfo['description'],
                'app_author_name' => $appInfo['developer']['name'],
                'app_author_url' => $appInfo['developer']['link']
            );

        $this->_db->insert( $table, $data );

        $ID = $this->_db->insertid();

        // add meta information
        $table = PREFIX . 'app_metas';
        $metas = array(
            'cover-image' => $appInfo['cover-image'],
            'rating-score' => $appInfo['rating']['score'],
            'reviews-num' => $appInfo['rating']['reviews-num'],
            'meta-info' => serialize( $appInfo['meta-info'] ),
            'what-new' => $appInfo['what-new'],
            'category' => serialize( $appInfo['category'] )
        );

        foreach ($metas as $key => $value) {
            
            $sql = "INSERT INTO " . $table . "(`app_id`, `meta_key`, `meta_value`) VALUES ( ". $ID .", '". $key ."', '". $value ."')";
            $this->_db->query( $sql );
        }

        // add taxonomy relationships
        $categorySlug = substr( $appInfo['category']['url'], 21);
        $sql = "INSERT INTO ". PREFIX ."taxonomy_relationships(term_id, object_id) VALUES (". $this->getTaxonomyIdBySlug( $categorySlug ) .", ". $ID .")";
        $this->_db->query( $sql );


        /*$table = PREFIX . 'app_comments';
        if ( isset( $appInfo['comments'] ) )
            foreach ($appInfo['comments'] as $key => $value) {
                
                $sql = "INSERT INTO " . $table . "(`app_id`, `author_name`, `content`) VALUES (". $ID .", '". $value['author_name'] ."', '". $value['content'] ."')";
                $this->_db->query( $sql );
            }*/
    }

    function getTaxonomyIdBySlug( $slug )
    {
        $sql = "SELECT id FROM ". PREFIX ."taxonomy WHERE slug = '". $slug ."'";
        $query = $this->_db->query( $sql );

        return $this->_db->fetch( $query )['id'];
    }
}
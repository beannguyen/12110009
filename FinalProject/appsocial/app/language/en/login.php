<?php

return array(
    '_username' => 'Username',
    '_email' => 'Email',
    '_password' => 'Password',
    '_login_in' => 'Login In',
    '_forgot_password' => 'Forgot your password?',
    '_forgot_password_btn' => 'Complete',
);
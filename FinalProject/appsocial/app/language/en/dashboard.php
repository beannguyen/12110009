<?php

return array(
    'welcome_message' => 'Hello, welcome from the welcome controller!',
    'welcome_lorem' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore, amet!',
    'view_homepage' => 'View Homepage',
    'dashboard_title' => 'Admin Dashboard',
    '_dashboard' => 'Dashboard',
    '_txt_url_format' => 'Index URL Formats',
    '_txt_url_format_page_title' => 'Index URL Formats',
    '_remember_me' => 'Remember me',
    '_login_out' => 'Logout',
);
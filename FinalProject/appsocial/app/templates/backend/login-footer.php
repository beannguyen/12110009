</div>


<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/gsap/main-gsap.js" id="script-resource-1"></script>
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js" id="script-resource-2"></script>
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/bootstrap.min.js" id="script-resource-3"></script>
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/joinable.js" id="script-resource-4"></script>
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/resizeable.js" id="script-resource-5"></script>
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/neon-api.js" id="script-resource-6"></script>
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/toastr.js" id="script-resource-6"></script>
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/jquery.validate.min.js" id="script-resource-7"></script>
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/neon-login.js" id="script-resource-8"></script>
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/neon-custom.js" id="script-resource-9"></script>
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/neon-demo.js" id="script-resource-10"></script>
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/generic.js" id="script-resource-10"></script>

</body>
</html>
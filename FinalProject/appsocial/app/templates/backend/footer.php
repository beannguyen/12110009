<!-- Footer -->
<footer class="main">

    2014 &copy; Develop by <strong>Mr.Bean</strong>

</footer>	</div>

</div>


<!-- Sample Modal (Skin gray) -->
<div class="modal fade" id="ajax-modal-dialog-1">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Ajax Modal</h4>
                <!--\\ Ajax Loader -->
                <div class="ajax-modal-loader">
                    <img class="loading-image" src="<?php echo \helpers\url::get_backend_template_path(); ?>assets/images/loader-1.gif" alt="loading..">
                </div>
                <!--\\ Ajax Loader -->
            </div>

            <div class="modal-body">
                <span>Loading...</span>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="modal_data_submit_btn" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="<?php echo \helpers\url::get_backend_template_path();?>assets/js/jvectormap/jquery-jvectormap-1.2.2.css"  id="style-resource-1">
<link rel="stylesheet" href="<?php echo \helpers\url::get_backend_template_path();?>assets/js/rickshaw/rickshaw.min.css"  id="style-resource-2">
<!-- CORE JS -->
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/gsap/main-gsap.js" id="script-resource-1"></script>
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js" id="script-resource-2"></script>
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/bootstrap.min.js" id="script-resource-3"></script>
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/joinable.js" id="script-resource-4"></script>
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/resizeable.js" id="script-resource-5"></script>
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/neon-api.js" id="script-resource-6"></script>
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/toastr.js" id="script-resource-6"></script>

<!-- CORE JS -->

<?php if(isset($data['js'])) : ?>

    <!-- JS plugins -->
    <?php foreach ($data['js'] as $row): ?>
        <script src="<?php echo \helpers\url::get_backend_template_path() . 'assets/' . $row; ?>"></script>
    <?php endforeach ?>

<?php endif ?>

<!-- CUSTOM SCRIPT -->
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/neon-chat.js" id="script-resource-13"></script>
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/neon-custom.js" id="script-resource-14"></script>
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/neon-demo.js" id="script-resource-15"></script>
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/generic.js" id="script-resource-10"></script>
<script src="<?php echo \helpers\url::get_backend_template_path();?>assets/js/underscore-min.js" id="script-resource-10"></script>

<?php if(isset($data['jq'])) : ?>

    <!-- JS scripts -->
    <script>
        $(document).ready(function(){
            <?php echo $data['jq']."\n";?>
        });
    </script>

<?php endif ?>

</body>
</html>
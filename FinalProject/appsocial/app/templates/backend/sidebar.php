<script type="text/javascript">
    jQuery(document).ready(function ($) {

        var activeClass = '<?php echo \helpers\session::get( 'active_sidebar_item' ); ?>';
        $('.' + activeClass).addClass( 'opened active' );
    });
</script>
<div class="sidebar-menu">

    <header class="logo-env">

        <!-- logo -->
        <div class="logo">
            <a href="<?php echo DIR; ?>dashboard">
                <img src="<?php echo \helpers\url::get_backend_template_path(); ?>assets/images/logo.png" alt=""/>
            </a>
        </div>

        <!-- logo collapse icon -->
        <div class="sidebar-collapse">
            <a href="#" class="sidebar-collapse-icon with-animation">
                <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                <i class="entypo-menu"></i>
            </a>
        </div>


        <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
        <div class="sidebar-mobile-menu visible-xs">
            <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                <i class="entypo-menu"></i>
            </a>
        </div>

    </header>


    <ul id="main-menu" class="">

        <li class="i-dashboard">
            <a href="<?php echo DIR; ?>dashboard"><i class="entypo-gauge"></i><span><?php echo $data['lang']['_dashboard']; ?></span></a>
        </li>
        <li class="i-url-formats">
            <a href="<?php echo DIR; ?>dashboard/url-format"><i class="entypo-flow-tree"></i><span><?php echo $data['lang']['_txt_url_format']; ?></span></a>
        </li>

    </ul>


</div>	
function editLinkFormat( linkId )
{
    $(".ajax-loader").fadeIn("slow");
    var datastring = 'get=1&link_id=' + linkId;

    $.ajax({
        type: "POST",
        url: getRootWebSitePath() + "/ajax-submit/url-format",
        data: datastring,
        async: false,
        success: function (responseText) {

            if (responseText !== 'error') {

                // decode json
                var data = JSON.parse( responseText );
                // set form data
                $('#url-formats-form #link_id').val(data.id);
                $('#url-formats-form #link_title').val(data.link_title);
                $('#url-formats-form #link_format').val(data.link_format);
                $('#url-formats-form #link_description').text(data.link_description);
                $('#url-formats-form #link_attribute').tagsinput('add', _.unescape(data.link_attribute));
                $('#url-formats-form #type').val(data.type);
                $('#url-formats-form #form_type').val('update');
                jQuery('#ajax-modal-dialog-1').modal('show', {backdrop: 'static'});
            }
        }
    });
    $(".ajax-loader").hide();
}

function deleteLinkFormat( linkId )
{
    $(".ajax-loader").fadeIn("slow");
    var datastring = 'delete=1&link_id=' + linkId;

    $.ajax({
        type: "POST",
        url: getRootWebSitePath() + "/ajax-submit/url-format",
        data: datastring,
        async: false,
        success: function (responseText) {

            if (responseText === 'deleted') {

                toastr.warning("The url was deleted.", "Deleted!", _optsWarning);
                setTimeout(function() {

                    window.location = $(location).attr('href');
                }, 2000);
            } else {

                toastr.error("Something went wrong, try again.", "Error!", _optsError);
                $(".ajax-loader").hide();
            }
        }
    });
}

var urlFormats = function () {

    return {
        init: function() {

            /* init datatable */
            $("#table-2").dataTable({
                "sPaginationType": "bootstrap",
                "sDom": "t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
                "bStateSave": false,
                "iDisplayLength": 8
            });

            $(".dataTables_wrapper select").select2({
                minimumResultsForSearch: -1
            });

            /* open modal setting */
            $('#fnAddRowBtn').click(function() {

                // set default form data
                $('#url-formats-form #link_title').val('');
                $('#url-formats-form #link_format').val('');
                $('#url-formats-form #link_description').text('');
                $('#url-formats-form #link_attribute').tagsinput('removeAll');
                $('#url-formats-form #type').val('');
                $('#url-formats-form #form_type').val('insert');
                jQuery('#ajax-modal-dialog-1').modal('show', {backdrop: 'static'});
            });

            /* load modifier url formats form */
            jQuery.ajax({
                url: "ajax/ajax-new-url-formats-form",
                success: function(response)
                {
                    jQuery('#ajax-modal-dialog-1 .modal-title').html('Add URL Formats');
                    jQuery('#ajax-modal-dialog-1 .modal-body').html(response);
                }
            });

            /* form submit */
            $('#ajax-modal-dialog-1 #modal_data_submit_btn').click(function() {

                $(".ajax-modal-loader").fadeIn("slow");

                var form_type = $('#form_type').val();
                var link_title = $('#link_title').val();
                var link_format = htmlspecialchars( $("#link_format").val() );
                var link_description = htmlspecialchars( $('#link_description').val() );
                var link_attribute = $('#link_attribute').val();
                var type = $('#type').val();

                var datastring = form_type + "=1&link_title=" + link_title + "&link_format=" + link_format + "&link_description=" + link_description + "&link_attribute=" + link_attribute + "&type=" + type;
                if ( form_type === 'update' ) {

                    var link_id = $('#link_id').val();
                    datastring += '&link_id=' + link_id;
                }
                $.ajax({
                    type: "POST",
                    url: getRootWebSitePath() + "/ajax-submit/url-format",
                    data: datastring,
                    async: false,
                    success: function (responseText) {

                        if (responseText === 'inserted') {

                            toastr.success("The data is inserted to database.", "Inserted!", _optsSuccess);
                            setTimeout(function() {

                                window.location = $(location).attr('href');
                            }, 2000);
                        } else if (responseText === 'updated') {

                            toastr.success("The data is updated to database.", "Updated!", _optsSuccess);
                            setTimeout(function() {

                                window.location = $(location).attr('href');
                            }, 2000);
                        } else {

                            toastr.error("Something went wrong, try again.", "Error!", _optsError);
                            $(".ajax-loader").hide();
                        }
                    }
                });
            });
        }
    }
}();

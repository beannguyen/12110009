<!DOCTYPE html>
<!--[if lte IE 8]>
<html class="no-js lt-ie9 fixed-layout" lang="en"> <![endif]-->
<!--[if IE 9]>
<html class="no-js ie9 fixed-layout" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html style=""
      class="fixed-layout js flexbox no-touch cssanimations audio localstorage svg placeholder xhr2 overthrow-enabled"
      lang="en"><!--<![endif]-->
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="chrome=1">
    <script src="<?php echo \helpers\url::get_template_path();?>js/core.js"></script>

    <title>
        <?php echo $data['title']; ?>
    </title>

    <meta content="The #1 marketplace for premium website templates, including themes for WordPress, Magento, Drupal, Joomla, and more.  Create a website, fast."
          name="description">

    <meta name="viewport" content="width=1024">
    <link rel="stylesheet" type="text/css" href="<?php echo \helpers\url::get_template_path();?>css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo \helpers\url::get_template_path();?>css/neon-core.css"/>
    <!--[if (gt IE 8)]><!-->
    <link href="<?php echo \helpers\url::get_template_path();?>css/index_static-4077d7931f6b413834712323cbd04f39.css" media="all" rel="stylesheet" type="text/css">
    <link href="<?php echo \helpers\url::get_template_path();?>css/index_static-c1a7ce2c0d715389240e1ee08036ce32.css" media="all" rel="stylesheet" type="text/css">
    <!--<![endif]-->

    <!--[if (lte IE 8)]>
    <link href="//dmypbau5frl9g.cloudfront.net/assets/market/core/index_ltie9-1a975614bbd0d00213e4e4a83279995a.css"
          media="all" rel="stylesheet" type="text/css"/>
    <link href="//dmypbau5frl9g.cloudfront.net/assets/market/pages/search/index_ltie9-2783b897e0366a5f0254e1288f546de7.css"
          media="all" rel="stylesheet" type="text/css"/>
    <![endif]-->


    <script src="<?php echo \helpers\url::get_template_path();?>js/jquery.js" type="text/javascript"></script>
    <script src="<?php echo \helpers\url::get_template_path();?>js/bootstrap.min.js" type="text/javascript"></script>
    <!-- <script src="<?php echo \helpers\url::get_template_path();?>js/head-5ca9ebe03f17ee5e0c944e52170cfbfe.js" type="text/javascript"></script> -->
    <script src="<?php echo \helpers\url::get_template_path();?>js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="<?php echo \helpers\url::get_template_path();?>js/toastr.js" type="text/javascript"></script>

    <style type="text/css">
        .fancybox-margin {
            margin-right: 0px;
        }
    </style>

    <link href="<?php echo \helpers\url::get_template_path();?>css/floatingPanel.css" type="text/css" rel="stylesheet">

</head>


<body id="" class="" data-view="app" data-responsive="false" data-user-signed-in="false">

<!--[if lte IE 7]>
<div style="color:#fff;background:#f00;padding:20px;text-align:center;">
    ThemeForest no longer actively supports this version of Internet Explorer. We suggest that you <a
        href="http://windows.microsoft.com/en-us/internet-explorer/download-ie"
        style="color:#fff;text-decoration:underline;">upgrade to a newer version</a> or <a
        href="http://browsehappy.com/" style="color:#fff;text-decoration:underline;">try a different browser</a>.
</div>
<![endif]-->


<div class="page">

    <div class="page__canvas">
        <div class="canvas">
            <div class="canvas__header">


                <header class="site-header">

                    <div class="site-header__logo-and-account ">
                        <div class="header-logo-account">
                            <div class="grid-container">
                                <div class="header-logo-account__container">
                                    <a class="header-logo-account__logo" href="<?php echo DIR; ?>">
                                        <span>AppSearch</span>
                                    </a>
                                    <nav class="header-logo-account__right">
                                    </nav>
                                </div>
                            </div>
                        </div>

                    </div>


                    </div>
                </header>
            </div>

            <div class="js-canvas__body canvas__body">


                <div class="page-section -theme-themeforest js-search-header search-header">
                    <div data-view="searchBar" data-facet-id="searchFacets">
                        <div class="grid--static">
                            <form id="search-form" class="huge-search--search-home" action="<?php echo DIR; ?>search" method="GET">
                                <input class="query-string" name="query" value="<?php if( isset( $_GET['query'] ) ) echo $_GET['query']; ?>" placeholder="Search within these results" type="search">
                                <span class="help-block"></span>
                                <button type="submit"><i class="glyphicon glyphicon-search"></i>
                                </button>
                            </form>
                        </div>

                        <div class="search-header__results-count">
                        </div>
                        <script type="text/javascript">
                            $( document ).ready(function() {
                            
                                $('#search-form').on('submit', function(e) {
                                    
                                    if ( $('#search-form .query-string').val() === '' ) {

                                        e.preventDefault();
                                        $('.help-block').val('Enter your keyword');
                                    }
                                })
                            });
                        </script>

                    </div>
                </div>


                <div class="content-main" id="content">

                    <div class="grid-container">
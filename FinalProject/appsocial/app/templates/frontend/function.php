<?php

use \helpers\generic;

class FrontFunc
{

	private $db;
	private $generic;

	function __construct()
	{
		//connect to PDO here.
		$this->db = \helpers\database::get();
        $this->db->connect();
        // Generic
        $this->generic = new Generic();
	}

	function loadSidebar( $type = 'category' )
	{
		$sql = "SELECT * FROM ". PREFIX ."taxonomy WHERE type = '". $type ."'";
		$query = $this->db->query( $sql );

		while ( $row = $this->db->fetch( $query ) ) {
			
			?>
			<li>
                <a href="<?php echo DIR; ?><?php echo $type; ?>/<?php echo $row['slug']; ?>" class="search-facet-categories__link">
                    <span class="search-facet-categories__label"><?php echo $row['name']; ?></span>
                </a>
            </li>
			<?php
		}
	}
}
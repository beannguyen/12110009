
<div class="sidebar-s">
    <form accept-charset="UTF-8" action="/search" class="js-facet-form" data-pjax=""
          data-view="facetingControls" id="searchFacets" method="get">
        <div style="margin:0;padding:0;display:inline"><input name="utf8" value="✓"
                                                              type="hidden"></div>
        <div data-view="searchFacetPanel">
            <input id="term" name="term" type="hidden">

            <div class="search-facet">
                <div class="search-facet-header">
                    <h2><span>Collection</span></h2>
                </div>


                <ul class="search-facet-categories">
                    <?php
                    include('function.php');
                    $func = new FrontFunc();
                    $func->loadSidebar('collection');
                    ?>
                </ul>


            </div>

            <div class="search-facet">
                <div class="search-facet-header">
                    <h2><span>Category</span></h2>
                </div>


                <ul class="search-facet-categories">
                    <?php
                    $func->loadSidebar('category');
                    ?>
                </ul>


            </div>
        </div>
    </form>


</div>
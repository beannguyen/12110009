</div>
                </div>


                <footer class="site-footer">
                    <div class="site-footer__primary">
                        <div class="footer-primary">
                            <div class="grid-container">

                                <div class="footer-top">

                                    <div class="footer-top__left">
                                    </div>

                                    <div class="footer-top__right is-hidden-tablet-and-below">


                                    </div>

                                </div>
                                <div class="footer-bottom">

                                    <p>
                                        <small>© 2014 | Developer by BeanNguyen
                                        </small>
                                    </p>

                                </div>


                            </div>
                        </div>

                    </div>
                </footer>

            </div>

            <div id="tooltip-magnifier" class="magnifier">
                <strong></strong>

                <div class="info">
                    <div class="author-category">
                        by <span class="author"></span>
                        <span class="category"></span>
                    </div>
                    <div class="price">
                        <span class="cost"></span>
                    </div>
                </div>
            </div>

            <div style="top: 1315px; left: 475px; display: none;" id="landscape-image-magnifier" class="magnifier">
                <div class="size-limiter"><img src="images/01.png"></div>
                <strong>Auto Car Repair Mechanic Shop Responsive Theme</strong>

                <div class="info">
                    <div class="author-category">
                        by <span class="author">vamtam</span>
                        <span class="category">WordPress / Corporate / Business</span>
                    </div>
                    <div class="price">
                        <span class="cost"><sup>$</sup>58</span>
                    </div>
                </div>
            </div>

            <div id="portrait-image-magnifier" class="magnifier">
                <div class="size-limiter">
                </div>
                <strong></strong>

                <div class="info">
                    <div class="author-category">
                        by <span class="author"></span>
                        <span class="category"></span>
                    </div>
                    <div class="price">
                        <span class="cost"></span>
                    </div>
                </div>
            </div>

            <div id="square-image-magnifier" class="magnifier">
                <div class="size-limiter">
                </div>
                <strong></strong>

                <div class="info">
                    <div class="author-category">
                        by <span class="author"></span>
                        <span class="category"></span>
                    </div>
                    <div class="price">
                        <span class="cost"></span>
                    </div>
                </div>
            </div>

            <div id="smart-image-magnifier" class="magnifier">
                <div class="size-limiter">
                </div>
                <strong></strong>

                <div class="info">
                    <div class="author-category">
                        by <span class="author"></span>
                        <span class="category"></span>
                    </div>
                    <div class="price">
                        <span class="cost"></span>
                    </div>
                </div>
            </div>

            <div id="video-magnifier" class="magnifier">
                <div class="size-limiter">
                    <div class="faux-player is-hidden"><img></div>
                    <div>
                        <div id="hover-video-preview"></div>
                    </div>
                </div>
                <strong></strong>

                <div class="info">
                    <div class="author-category">
                        by <span class="author"></span>
                        <span class="category"></span>
                    </div>
                    <div class="price">
                        <span class="cost"></span>
                    </div>
                </div>
            </div>

        </div>


        <div class="page__overlay" data-view="offCanvasNavToggle" data-off-canvas="close"></div>
    </div>
</div>

<div style="bottom: auto; left: 781px; right: auto; top: 251px; display: none;" class="translator-theme-default"
     id="translator-floating-panel">
    <div title="Click to translate" id="translator-floating-panel-button"></div>
</div>

<?php if(isset($data['js'])) : ?>

    <!-- JS plugins -->
    <?php foreach ($data['js'] as $row): ?>
        <script src="<?php echo $row; ?>"></script>
    <?php endforeach ?>

<?php endif ?>

<?php if(isset($data['jq'])) : ?>

    <!-- JS scripts -->
    <script>
        $(document).ready(function(){
            <?php echo $data['jq']."\n";?>
        });
    </script>

<?php endif ?>

</body>

</html>
<div class="js-faceted-search" data-view="pjaxFacetedSearch">
    <div class="content-l content-right">

        <ul class="item-list" data-view="bookmarkStatesLoader">
            
            <?php

                if ( sizeof( $data['apps'] ) > 0 )
                {
                    foreach ($data['apps'] as $key => $value) {
                        
                        if ( $key === 'navigation')
                            continue;
                        ?>
                        <li data-item-id="<?php echo $data['apps']['app_id']; ?>">


                            <div class="thumbnail thumbnail--has-actions">
                                <a href="https://play.google.com/store/apps/details?id=<?php echo $value['app_id']; ?>" target="_blank">
                                    <img alt="<?php echo $value['app_title']; ?>"
                                        src="<?php echo $value['cover-image']; ?>"
                                        title="<?php echo $value['app_title']; ?>" width="80"
                                        border="0" height="80">
                                </a>
                            </div>


                            <div class="item-info">
                                <h3>
                                    <a href="https://play.google.com/store/apps/details?id=<?php echo $value['app_id']; ?>" target="_blank">
                                        <?php echo $value['app_title']; ?>
                                    </a>
                                </h3>

                                <a href="https://play.google.com<?php echo $value['app_author_url']; ?>" class="author" target="_blank"><?php echo $value['app_author_name']; ?></a>
                            </div>

                            <small class="meta">
                                <span class="meta-categories">in
                                    <?php
                                        $category = unserialize( $value['category'] );
                                    ?> 
                                    <a href="https://play.google.com<?php echo $category['url'] ?>" target="_blank"><?php echo $category['name'] ?></a>
                                </span>

                                <br>

                                <?php 
                                    echo \helpers\generic::split_words( $value['app_description'], 400, '...')
                                ?>
                            </small>


                        </li>
                        <?php
                    }
                } else {

                }
            ?>

        </ul>


        <nav class="pagination is-hidden-tablet-and-below js-pagination">
            <?php echo $data['apps']['navigation']; ?>
            <!--<ul class="pagination__list">
                <li>
                    <span class="pagination__previous--disabled js-pagination-previous ">
                        <b>Prev</b>
                    </span>
                </li>
                <li>
                    <span class="pagination__summary">Page 1 of 60</span>
                </li>
                <li>
                    <span class="pagination__page--current">1</span>
                </li>
                <li>
                    <a rel="next" class="pagination__page" href="http://themeforest.net/search?_pjax=.js-faceted-search&amp;page=2">2</a>
                </li>
                <li>
                    <a class="pagination__page" href="http://themeforest.net/search?_pjax=.js-faceted-search&amp;page=3">3</a>
                </li>
                <li>
                    <a class="pagination__page" href="http://themeforest.net/search?_pjax=.js-faceted-search&amp;page=4">4</a>
                </li>
                <li>
                    <a class="pagination__page" href="http://themeforest.net/search?_pjax=.js-faceted-search&amp;page=5">5</a>
                </li>
                <li>
                    <span class="pagination__gap">…</span>
                </li>
                <li>
                    <a class="pagination__page" href="http://themeforest.net/search?_pjax=.js-faceted-search&amp;page=60">60</a>
                </li>
                <li>
                    <a class="pagination__next js-pagination-next" rel="next" href="http://themeforest.net/search?_pjax=.js-faceted-search&amp;page=2"><b>Next</b></a>
                </li>
            </ul> -->
        </nav>


                            </div>

                                <!-- SIDEBAR -->

                                <?php require_once( \helpers\url::get_path() . 'sidebar.php'); ?>
                            
                                <!--\\ SIDEBAR -->


                            <div class="js-hidden-search-bar hidden">


                                <div class="page-section -theme-themeforest js-search-header search-header">
                                    <div data-view="searchBar" data-facet-id="searchFacets">
                                        <div class="grid--static">
                                            <form class="search-home" action="/search" method="GET">
                                                <input name="term" placeholder="Search within these results"
                                                       type="search">
                                                <button type="submit"><i class="glyphicon glyphicon-search"></i>
                                                </button>
                                            </form>
                                        </div>

                                        <div class="search-header__results-count">
                                            17,258
                                            <h1>Templates &amp; Themes</h1>
                                        </div>

                                    </div>
                                </div>


                            </div>

                            <div class="js-hidden-title hidden">
                                Search Templates &amp; Themes | ThemeForest
                            </div>

                            <div data-view="userSatisfactionSurvey" data-survey-id="si-17-44"
                                 data-survey-username="(anonymous)" id="userSatisfactionSurvey"></div>


                        </div>
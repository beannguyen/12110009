<?php

use \helpers\generic;

class ThemeFunc
{

	private $db;
	private $generic;
	private $_mApp;

	function __construct()
	{
		//connect to PDO here.
		$this->db = \helpers\database::get();
        $this->db->connect();
        // Generic
        $this->generic = new Generic();

        // model
        $this->_mApp = new \models\apps();
	}

	function load( $listId )
	{
		if ( sizeof( $listId ) > 0 )
                {
                    foreach ($listId as $key => $value) {
                        
                        if ( $key === 'navigation')
                            continue;

                        $sql = "SELECT * FROM " . PREFIX . "apps_indexed WHERE id = " . $value;
                        $query = $this->db->query( $sql );

                        while ( $row = $this->db->fetch( $query ) ) {
                        	
                        	$meta = $this->_mApp->getAppMetas( $row['id'] );
                        	?>

		                        <li data-item-id="<?php echo $row['app_id']; ?>">


		                            <div class="thumbnail thumbnail--has-actions">
		                                <a href="https://play.google.com/store/apps/details?id=<?php echo $row['app_id']; ?>" target="_blank">
		                                    <img alt="<?php echo $row['app_title']; ?>"
		                                        src="<?php echo $meta['cover-image']; ?>"
		                                        title="<?php echo $row['app_title']; ?>" width="80"
		                                        border="0" height="80">
		                                </a>
		                            </div>


		                            <div class="item-info">
		                                <h3>
		                                    <a href="https://play.google.com/store/apps/details?id=<?php echo $row['app_id']; ?>" target="_blank">
		                                        <?php echo $row['app_title']; ?>
		                                    </a>
		                                </h3>

		                                <a href="https://play.google.com<?php echo $row['app_author_url']; ?>" class="author" target="_blank"><?php echo $row['app_author_name']; ?></a>
		                            </div>

		                            <small class="meta">
		                                <span class="meta-categories">in
		                                    <?php
		                                        $category = unserialize( $meta['category'] );
		                                    ?> 
		                                    <a href="https://play.google.com<?php echo $category['url'] ?>" target="_blank"><?php echo $category['name'] ?></a>
		                                </span>

		                                <br>

		                                <?php 
		                                    echo \helpers\generic::split_words( $row['app_description'], 400, '...')
		                                ?>
		                            </small>


		                        </li>
                        <?php
                        }
                    }
                } else {

                }
	}
}
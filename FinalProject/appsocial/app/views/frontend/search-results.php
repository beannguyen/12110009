<div class="js-faceted-search" data-view="pjaxFacetedSearch">
    <div class="content-l content-right">

        <ul class="item-list" data-view="bookmarkStatesLoader">
            
            <?php
                include('function.php');
                $themeFunc = new ThemeFunc();
                $themeFunc->load( $data['results'] );
            ?>

        </ul>


        <nav class="pagination is-hidden-tablet-and-below js-pagination">
            <?php echo $data['results']['navigation']; ?>
        </nav>


                            </div>

                                <!-- SIDEBAR -->

                                <?php require_once( \helpers\url::get_path() . 'sidebar.php'); ?>
                            
                                <!--\\ SIDEBAR -->


                            <div class="js-hidden-search-bar hidden">


                                <div class="page-section -theme-themeforest js-search-header search-header">
                                    <div data-view="searchBar" data-facet-id="searchFacets">
                                        <div class="grid--static">
                                            <form class="search-home" action="/search" method="GET">
                                                <input name="term" placeholder="Search within these results"
                                                       type="search">
                                                <button type="submit"><i class="glyphicon glyphicon-search"></i>
                                                </button>
                                            </form>
                                        </div>

                                    </div>
                                </div>


                            </div>
                        </div>
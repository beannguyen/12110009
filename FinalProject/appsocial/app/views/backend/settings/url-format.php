<ol class="breadcrumb bc-3">
    <li>
        <a href="<?php echo DIR . '/dashboard'; ?>"><i class="entypo-home"></i>Home</a>
    </li>
    <li class="active">

        <strong>Index URL Formats</strong>
    </li>
</ol>

<br/>

<a href="javascript:;" id="fnAddRowBtn" class="btn btn-primary">
    <i class="entypo-plus"></i>
    Add Row
</a>
<br/>
<br/>
<table class="table table-bordered table-striped datatable" id="table-2">
<thead>
<tr>
    <th>#</th>
    <th>Title</th>
    <th>Format</th>
    <th>Attribute</th>
    <th>Description</th>
    <th>Type</th>
    <th></th>
</tr>
</thead>

<tbody>
<?php
$db = \helpers\database::get();
$db->connect();

$sql = "SELECT * FROM ". PREFIX ."search_url_formed";
$query = $db->query( $sql );

$i = 1;
while ( $row = $db->fetch( $query ) ) {

    ?>
    <tr>
        <td><?php echo $i; $i++; ?></td>
        <td><?php echo $row['link_title']; ?></td>
        <td><?php echo $row['link_format']; ?></td>
        <td>
            <?php
            $array = explode(',', $row['link_attribute']);
            foreach ( $array as $v ) {

                echo $v . ', ';
            }
            ?>
        </td>
        <td><?php echo $row['link_description']; ?></td>
        <td><?php echo $row['type']; ?></td>
        <td>
            <a href="javascript:;" onclick='editLinkFormat(<?php echo $row['id']; ?>)' id="edit_action_btn" class="btn btn-default">
                <i class="entypo-pencil"></i>
            </a>

            <a href="javascript:;" onclick='deleteLinkFormat(<?php echo $row['id']; ?>)' id="delete_action_btn" class="btn btn-danger">
                <i class="entypo-cancel"></i>
            </a>
        </td>
    </tr>
<?php
}
?>

</tbody>
</table>

<div class="login-header login-caret">

    <div class="login-content">

        <a href="#" class="logo">
            <img src="<?php echo \helpers\url::get_backend_template_path();?>assets/images/logo.png" alt="" />
        </a>

        <!-- progress bar indicator -->
        <div class="login-progressbar-indicator">
            <h3>43%</h3>
            <span>logging in...</span>
        </div>
    </div>

</div>

<div class="login-progressbar">
    <div></div>
</div>

<div class="login-form">

    <div class="login-content">

        <form method="post" role="form" id="form_login">

            <div class="form-group">

                <div class="input-group">
                    <div class="input-group-addon">
                        <?php echo ( $data['use_emails'] == 1 ) ? '<i class="entypo-mail"></i>' : '<i class="entypo-user"></i>'; ?>
                    </div>

                    <input type="text" class="form-control" name="<?php echo ( $data['use_emails'] == 1 ) ? 'email' : 'username'; ?>" id="<?php echo ( $data['use_emails'] == 1 ) ? 'email' : 'username'; ?>" placeholder="<?php echo ( $data['use_emails'] == 1 ) ? $data['lang']['_email'] : $data['lang']['_username']; ?>" autocomplete="off" />
                </div>

            </div>

            <div class="form-group">

                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="entypo-key"></i>
                    </div>

                    <input type="password" class="form-control" name="password" id="password" placeholder="<?php echo $data['lang']['_password']; ?>" autocomplete="off" />
                </div>

            </div>

            <div class="form-group">
                <div class="checkbox checkbox-replace color-green">
                    <input type="checkbox" id="remember" name="remember" />
                    <label>Remember me?</label>
                </div>
            </div>
            <input type="hidden" id="token" name="token" value="<?php echo \helpers\Session::get('token'); ?>"/>
            <input type="hidden" name="login" value="1"/>
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block btn-login"><?php echo $data['lang']['_login_in']; ?> <span class="login_loading"></span></button>
            </div>

        </form>


        <div class="login-bottom-links">

            <a href="<?php echo DIR . 'login/forgot'; ?>" class="link"><?php echo $data['lang']['_forgot_password']; ?></a>

        </div>

        <div class="loader" style="display: none;">
            <img src="<?php echo \helpers\url::get_backend_template_path(); ?>assets/images/loader-1.gif" />
        </div>

    </div>

</div>
	
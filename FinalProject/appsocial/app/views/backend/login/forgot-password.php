
<div class="login-header login-caret">

    <div class="login-content">

        <a href="#" class="logo">
            <img src="<?php echo \helpers\url::get_backend_template_path();?>assets/images/logo.png" alt="" />
        </a>

        <!-- progress bar indicator -->
        <div class="login-progressbar-indicator">
            <h3>43%</h3>
            <span>logging in...</span>
        </div>
    </div>

</div>

<div class="login-progressbar">
    <div></div>
</div>

<div class="login-form">

    <div class="login-content">

        <form method="post" role="form" id="form_forgot_password">

            <div class="form-group">

                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="entypo-mail"></i>
                    </div>

                    <input type="email" class="form-control" name="email-reset-password" id="email-reset-password" placeholder="<?php echo $data['lang']['_email']; ?>" autocomplete="off" />
                </div>

            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block btn-login"><?php echo $data['lang']['_forgot_password_btn']; ?> <span class="login_loading"></span></button>
            </div>

            <div class="loader" style="display: none;">
                <img src="<?php echo \helpers\url::get_backend_template_path(); ?>assets/images/loader-1.gif" />
            </div>

        </form>

    </div>

</div>
	
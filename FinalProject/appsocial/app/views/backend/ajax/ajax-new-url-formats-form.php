<form id="url-formats-form" role="form" class="form-horizontal form-groups validate">

    <div class="form-group">
        <label for="field-1" class="col-sm-3 control-label">Title</label>

        <div class="col-sm-8">
            <input class="form-control" id="link_title" data-validate="required" data-message-required="This is a required field." placeholder="Link title" type="text">
        </div>
    </div>

    <div class="form-group">
        <label for="field-1" class="col-sm-3 control-label">Link Format</label>

        <div class="col-sm-8">
            <input class="form-control" id="link_format" data-validate="required" data-message-required="This is a required field." placeholder="Format" type="text">
        </div>
    </div>

    <div class="form-group">
        <label for="field-1" class="col-sm-3 control-label">Description</label>

        <div class="col-sm-8">
            <textarea class="form-control autogrow" id="link_description"></textarea>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Attribute</label>

        <div class="col-sm-8">

            <input type="text" value="" id="link_attribute" class="form-control link_filter" data-validate="required" data-message-required="This is a required field." />

        </div>
    </div>

    <div class="form-group">
        <label for="field-1" class="col-sm-3 control-label">Type</label>

        <div class="col-sm-8">
            <input class="form-control" id="type" placeholder="Type" type="text" data-validate="required" data-message-required="This is a required field." />
        </div>
    </div>
    <input type="hidden" id="form_type" value="insert" />
    <input type="hidden" id="link_id" />
    <script type="text/javascript">
        $( document ).ready(function() {

            /* init form tagsinput */
            $(".link_filter").tagsinput();
        });
    </script>
</form>
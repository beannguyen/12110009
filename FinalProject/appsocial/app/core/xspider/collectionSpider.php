<?php 
namespace core\xspider;

use \core\xspider\caspercore;
use \helpers\generic;

class CollectionSpider
{
	protected $url;
	protected $start;
	protected $num;
	protected $outfile;
	protected $casper;
	protected $generic;

	function __construct()
	{
		$this->url = '';
		$this->start = 0;
		$this->num = 60;

		$this->casper = new Caspercore();
		$this->generic = new Generic();

        // set options
        $this->casper->setOptions(array(
            '--web-security' => 'no',
            '--ignore-ssl-errors' => 'yes'
        ));
	}

	public function setUrl( $url )
	{
		$this->url = $url;
	}

	public function setStart( $start )
	{
		$this->start = $start;
	}

	public function setNum( $num )
	{
		$this->num = $num;
	}

	public function setOutput( $filename )
	{
		$this->outfile = $filename;
	}

	public function execute()
	{
		$this->casper->start();

		$data = array(
				'start' => $this->start,
				'num' => $this->num,
				'numChildren' => 0,
				'ipf' => 1,
				'xhr'=> 1
			);
		$this->casper->open($this->url, 'post', json_encode( $data ));

		$this->casper->then("
            
	            links = this.evaluate(function() {
	                var elements = __utils__.findAll('div.card');
	                return elements.map(function(e) {
	                    return e.getAttribute('data-docid');
	                });
	            });
        ");

        // run the casper script
        $this->casper->run("function(){

            this.echo( links ).exit();
        }");
        
        // need to debug? just check the casper output
        foreach ($this->casper->getOutput() as $outLine) {
            
            $outJson = json_encode( $outLine );

            $storeFolder = '/tmp/storage/';
            $today = getdate();
            $storeFolder .= $today["year"] . '/' . $today['mon'] . '/' . $today['mday'] . '/';
            if ( !file_exists( $storeFolder ) )
                $this->generic->rmkdir($storeFolder);

            $outputFile = 'collection_' . $this->outfile . '_' . $start;

            if ( file_put_contents($storeFolder .  $outputFile . '.json', $outJson) )
                echo 'write data to file success';
        }
	}
}
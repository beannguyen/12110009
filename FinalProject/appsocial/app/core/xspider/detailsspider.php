<?php 
namespace core\xspider;

use Goutte\Client;
use \core\xspider\caspercore;
use \helpers\generic;

class Detailsspider
{
	/**
     * The base URL from which the crawler begins crawling
     * @var string
     */
    protected $baseUrl;
    /**
     * Array of links (and related data) found by the crawler
     * @var array
     */
    protected $links;
    /**
     * Constructor
     * @param string $baseUrl
     * @param int $maxDepth
     */
    public function __construct($baseUrl)
    {
        $this->generic = new Generic();
        $this->baseUrl = $baseUrl;
        $this->links = array();
    }

    /**
     * Initiate the crawl
     * @param string $url
     */
    public function traverse($url = null)
    {
        if ($url === null) {
            $url = $this->baseUrl;
            $this->links = array(
                'links_text' => array('BASE_URL'),
                'absolute_url' => $url,
                'frequency' => 1,
                'visited' => false,
                'external_link' => false,
                'original_urls' => array($url)
            );
        }
        $this->traverseSingle($url, $this->maxDepth);
    }

    /**
     * Get links (and related data) found by the crawler
     * @return array
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * Crawl single URL
     * @param string $url
     * @param int $depth
     */
    protected function traverseSingle($url, $depth)
    {
        try {
            $client = new Client();
            $client->followRedirects();
            $crawler = $client->request('GET', $url);
            $statusCode = $client->getResponse()->getStatus();
            $hash = $this->getPathFromUrl($url);
            $this->links[$hash]['status_code'] = $statusCode;
            if ($statusCode === 200) {
                $content_type = $client->getResponse()->getHeader('Content-Type');
                if (strpos($content_type, 'text/html') !== false) { //traverse children in case the response in HTML document only

                    $this->extractInfo($crawler, $hash);
                    $this->links[$hash]['visited'] = true;
                }
            }
        } catch (\Guzzle\Http\Exception\CurlException $e) {
            $this->links['status_code'] = '404';
            $this->links['error_code'] = $e->getCode();
            $this->links['error_message'] = $e->getMessage();
        } catch (\Exception $e) {
            $this->links['status_code'] = '404';
            $this->links['error_code'] = $e->getCode();
            $this->links['error_message'] = $e->getMessage();
        }
    }

    /**
     * Extract title information from url
     * @param \Symfony\Component\DomCrawler\Crawler $crawler
     * @param string $url
     */
    protected function extractInfo(\Symfony\Component\DomCrawler\Crawler $crawler, $url)
    {
    	// get cover image
    	$this->links['cover-image'] = trim( $crawler->filter('.apps .details-info .cover-image, .devices .details-info .cover-image')->attr('src'));
    	// get app title
        $this->links['title'] = trim($crawler->filter('.info-container .document-title')->text());
        // get app category
        $this->links['category']['name'] = trim( str_replace("'", '', $crawler->filter('.info-container a.document-subtitle.category')->text() ) );
        $this->links['category']['url'] = trim( str_replace("'", '', $crawler->filter('.info-container a.document-subtitle.category')->attr('href') ) );
        // get developer name
        $this->links['developer']['name'] = trim($crawler->filter('.info-container a.document-subtitle.primary')->text());
        // get developer link
        $this->links['developer']['link'] = trim($crawler->filter('.info-container a.document-subtitle.primary')-> attr( 'href' ));
        // description
        $this->links['description'] = trim( str_replace("'", '', $crawler->filter('.id-app-orig-desc')->text() ) );
        // get score rating
        $this->links['rating']['score'] = trim($crawler->filter('.score-container .score')->text());
        // get number of reviews
        $this->links['rating']['reviews-num'] = trim($crawler->filter('.score-container .reviews-stats .reviews-num')->text());
        // get meta information
        $crawler->filter('.meta-info .content')->each(function (\Symfony\Component\DomCrawler\Crawler $node, $i) use ($url) {
        	$this->links['meta-info'][$i] = trim($node->text());	
        });
        // get comments
        /*$crawler->filter('.single-review')->each(function ($node, $i) {

        	$this->links['comments'][$i]['author_name'] = trim( $node->filter('.review-info .author-name')->text() );
        	$this->links['comments'][$i]['content'] = trim( str_replace("'", '', $node->filter('.review-body')->text() ) );
        });*/

        // get what new
        $crawler->filter('.whatsnew .recent-change')->each(function ( $node, $i ) {

        	$this->links['what-new'] .= '<li>' . trim( str_replace("'", '', $node->text() ) ) . '</li>';
        });


        // get related apps
        $this->getRalated( $url );

    }

    private function getRalated( $url )
    {
        $casper = new caspercore();

        // set options
        $casper->setOptions(array(
            '--web-security' => 'no',
            '--ignore-ssl-errors' => 'yes'
        ));

        $casper->start($url);

        $casper->then("
            
            links = this.evaluate(function() {
                var elements = __utils__.findAll('div.card');
                return elements.map(function(e) {
                    return e.getAttribute('data-docid');
                });
            });

        ");

        // run the casper script
        $casper->run("function(){

            this.echo( links ).exit();
        }");

        var_dump($casper->getOutput());

        // need to debug? just check the casper output
        foreach ($casper->getOutput() as $outLine) {
            
            $outJson = json_encode( $outLine );
            var_dump( $outJson );

            $storeFolder = '/tmp/storage/';
            $today = getdate();
            $storeFolder .= $today["year"] . '/' . $today['mon'] . '/' . $today['mday'] . '/';
            if ( !file_exists( $storeFolder ) )
                $this->generic->rmkdir($storeFolder);

            $outputFile = 'related_' . uniqid();

            if ( file_put_contents($storeFolder .  $outputFile . '.json', $outJson) )
                echo 'write data to file success';
        }
    }

    /**
     * extrating the relative path from url string
     * @param type $url
     * @return type
     */
    protected function getPathFromUrl($url)
    {
        if (strpos($url, $this->baseUrl) === 0 && $url !== $this->baseUrl) {
            return str_replace($this->baseUrl,'', $url);
        } else {
            return $url;
        }
    }
}
<?php namespace core;

/*
 * config - an example for setting up system settings
 * When you are done editing, rename this file to 'config.php'
 *
 * @version 2.1
 * @date June 27, 2014
 */
class Config {

	public function __construct() {

		//turn on output buffering
		ob_start();

		//site address
		define('DIR', 'http://localhost/appsocial/');
		define('ROOT', $_SERVER['DOCUMENT_ROOT'] . '/appsocial' );

		//set default controller and method for legacy calls
		define('DEFAULT_CONTROLLER', 'welcome');
		define('DEFAULT_METHOD' , 'index');

		//set a default language
		define('LANGUAGE_CODE', 'en');

		//database details ONLY NEEDED IF USING A DATABASE
		define('DB_TYPE', 'mysql');
		define('DB_HOST', 'localhost');
		define('DB_NAME', 'appsocial');
		define('DB_USER', 'root');
		define('DB_PASS', '');
		define('PREFIX', 'ase_'); // Applications Search Engine

		//set prefix for sessions
		define('SESSION_PREFIX', 'ase_');

		//optionall create a constant for the name of the site
		define('SITETITLE', 'AppSocial');

		//turn on custom error handling
		set_exception_handler('core\logger::exception_handler');
		set_error_handler('core\logger::error_handler');

		//set timezone
		date_default_timezone_set('Asia/Ho_Chi_Minh');

		//start sessions
		\helpers\session::init();

		//set the default template
		\helpers\session::set('frontend_template', 'frontend');
		\helpers\session::set('backend_template', 'backend');
	}

}

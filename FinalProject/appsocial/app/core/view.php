<?php namespace core;

/*
 * View - load template pages
 */
class View {

	/**
	 * include template file
	 * @param  string  $path  path to file from views folder
	 * @param  bool|array $data  array of data
	 * @param  bool|array $error array of errors
	 */
	public static function render($path,$data = false, $error = false){
		require "app/views/$path.php";
	}

    /**
     * return absolute path to selected template directory
     * @param  string  $path  path to file from views folder
     * @param  bool|array $data  array of data
     */
    public static function rendertemplate($path, $data = false){
		require "app/templates/". \helpers\Session::get('frontend_template') ."/$path.php";
	}

    /**
     * return absolute path to selected template directory
     * @param  string  $path  path to file from views folder
     * @param  bool|array $data  array of data
     */
    public static function renderbackendtemplate($path, $data = false){
        require "app/templates/". \helpers\Session::get('backend_template') ."/$path.php";
    }
	
}
<?php namespace controllers;

class Ajax extends \core\controller
{
    private $_mAjax;
    function __construct()
    {
        parent::__construct();
        $this->_mAjax = new \models\ajax();
    }

    function submit( $query )
    {
        if ( $query === 'url-format' ) {

            if ( isset( $_POST ) ) {

                if ( isset( $_POST['insert'] ) ) {

                    $this->insert_url_format();
                } elseif ( isset( $_POST['get'] ) ) {

                    $res = $this->_mAjax->get_url_format( $_POST['link_id'] );
                    echo json_encode( $res );
                } elseif ( isset( $_POST['update'] ) ) {

                    $this->update_url_format();
                } elseif ( isset( $_POST['delete'] ) ) {

                    echo $this->_mAjax->delete_url_format( $_POST['link_id'] );
                }
            }
        }
    }

    /**
     * Insert url format controller
     * @return bool
     */
    private function insert_url_format()
    {
        // get data
        $data = array();
        foreach ( $_POST as $k => $v ) {

            if ( $k !== 'insert' ) {

                $data[$k] = $this->generic->secure( $v );
            }
        }

        if ( $this->_mAjax->url_format( 'insert', $data ) )
            echo 'inserted';
        else
            echo 'failed';
        return false;
    }

    function update_url_format()
    {
        // get data
        $data = array();
        foreach ( $_POST as $k => $v ) {

            if ( $k !== 'update' && $k !== 'link_id' ) {

                $data[$k] = $this->generic->secure( $v );
            }
        }

        if ( $this->_mAjax->url_format( 'update', $data, $_POST['link_id'] ) )
            echo 'updated';
        else
            echo 'failed';
        return false;
    }
}
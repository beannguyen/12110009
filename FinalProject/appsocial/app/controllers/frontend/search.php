<?php
namespace controllers\frontend;

use core\view;

class Search extends \core\controller
{
	private $_mApp;

	function __construct()
	{
		parent::__construct();
		$this->_mApp = new \models\apps();

		// load language
		$this->language->load('frontend');
	}

	public function results()
	{
		$query = $_GET['query'];
		$data['title'] = 'Result for: ' . $query;
		$data['results'] = $this->_mApp->getSearchResults( $query );
		//var_dump( $data['apps'] );

		View::rendertemplate('header', $data);
		View::render('frontend/search-results', $data);
		View::rendertemplate('footer', $data);
	}
} 
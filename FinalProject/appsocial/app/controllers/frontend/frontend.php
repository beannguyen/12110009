<?php
namespace controllers\frontend;

use core\view;

class Frontend extends \core\controller
{
	private $_mApp;

	function __construct()
	{
		parent::__construct();
		$this->_mApp = new \models\apps();

		// load language
		$this->language->load('frontend');
	}

	public function index()
	{
		$data['title'] = 'AppSearch';
		$data['apps'] = $this->_mApp->getAllApps();
		//var_dump( $data['apps'] );

		View::rendertemplate('header', $data);
		View::render('frontend/index', $data);
		View::rendertemplate('footer', $data);
	}
} 

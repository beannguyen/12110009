<?php
namespace controllers\frontend;

use core\view;

class Taxonomy extends \core\controller
{
	private $_mApp;
	private $_mTax;

	function __construct()
	{
		parent::__construct();
		$this->_mApp = new \models\apps();
		$this->_mTax = new \models\taxonomy();

		// load language
		$this->language->load('frontend');
	}

	public function collection( $slug )
	{
		// get collection info
		$collection = $this->_mTax->getTaxonomyInfo( $slug );

		$data['title'] = 'Archive: ' . $collection['name'];
		$data['apps'] = $this->_mTax->getTaxonomyApps( $collection['id'] );
		//var_dump( $data['apps'] );

		View::rendertemplate('header', $data);
		View::render('frontend/taxonomy', $data);
		View::rendertemplate('footer', $data);
	}

	public function category( $slug )
	{
		$category = $this->_mTax->getTaxonomyInfo( $slug );
		$data['title'] = 'Archive: ' . $category['name'];
		$data['apps'] = $this->_mTax->getTaxonomyApps( $category['id'] );
		//var_dump( $data['apps'] );

		View::rendertemplate('header', $data);
		View::render('frontend/taxonomy', $data);
		View::rendertemplate('footer', $data);
	}
} 
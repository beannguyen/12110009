<?php 

namespace controllers\bots;

use \core\xspider\collectionSpider;
use \core\xspider\detailsspider;
use \helpers\generic;

class Bots extends \core\controller
{
    private $_mBot;
    function __construct()
    {
        parent::__construct();
        $this->_mBot = new \models\bots;
    }

    function collection()
    {
        $crawler = new CollectionSpider();

        $requestLinks = $this->_mBot->getLinks( 'collection' );

        foreach ($requestLinks as $key => $value) {
            
            $crawler->setUrl( $value );
            $start = 0;
            $num = 60;
            while ( $start < 540 ) {
                
                $crawler->setStart( $start );
                $crawler->setNum( $num );
                $crawler->setOutput( $key . '_' . $start );
                $crawler->execute();

                $start += $num;
            }
        }
    }

    function category()
    {
        $crawler = new CollectionSpider();

        $requestLinks = $this->_mBot->getLinks( 'category' );

        foreach ($requestLinks as $key => $value) {
            
            $crawler->setUrl( $value );
            $start = 0;
            $num = 60;
            while ( $start < 540 ) {
                
                $crawler->setStart( $start );
                $crawler->setNum( $num );
                $crawler->setOutput( $key . '_' . $start );
                $crawler->execute();

                $start += $num;
            }
        }
    }

    function details()
    {

        // allow file
        $allowext = array("json");
        // target path
        $today = getdate();
        $targetPath = '/tmp/storage/';
        $targetPath .= $today["year"] . '/' . $today['mon'] . '/' . $today['mday'] . '';
        // get list files
        $files = $this->generic->scanDirectories($targetPath, $allowext);
        
        if ( !$files )
            return false;

        foreach ($files as $key => $filename) {
            
            // get content
            $packages = json_decode(file_get_contents( $filename ));
            // remove file
            unlink( $filename );
            // convert to array
            $packages = explode(',', $packages);

            if ( empty( $packages ) )
                return false;

            foreach ($packages as $key => $package) {

                if ( !$this->_mBot->isIndexed( $package ) ) {

                    // create url to crawl
                    $url = 'https://play.google.com/store/apps/details?id=' . $package;
                    // crawling...
                    $bot = new Detailsspider( $url );
                    $bot->traverse();
                    $appInfo = $bot->getLinks();
                    $appInfo['package'] = $package;

                    // insert to db
                    $this->_mBot->insertAppInfo( $appInfo );
                }
            }
        }
    }
}

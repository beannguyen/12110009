<?php namespace controllers\backend;
use core\view,
    helpers\url;
use helpers\Session;

class Dashboard extends \core\controller{

    private $lang = array();
    /**
     * call the parent construct
     */
    public function __construct(){
        parent::__construct();

        if ( !$this->generic->guestOnly() )
            Url::redirect('login');

        // Call all lang string
        $this->language->load('dashboard');
        $this->lang['view_homepage'] = $this->language->get('view_homepage');
        $this->lang['dashboard_title'] = $this->language->get('dashboard_title');
        $this->lang['_dashboard'] = $this->language->get('_dashboard');
        $this->lang['_txt_url_format'] = $this->language->get('_txt_url_format');
        $this->lang['_txt_url_format_page_title'] = $this->language->get('_txt_url_format_page_title');
        $this->lang['_login_out'] = $this->language->get('_login_out');
    }

    /**
     * define page title and load template files
     */
    public function index() {

        // dashboard title
        $data['title'] = $this->lang['dashboard_title'];
        $data['lang'] = $this->lang;

        // active sidebar item
        Session::set( 'active_sidebar_item', 'i-dashboard' );

        // LOAD JS
        $data['js'][] = 'js/jvectormap/jquery-jvectormap-1.2.2.min.js';
        $data['js'][] = 'js/jvectormap/jquery-jvectormap-europe-merc-en.js';
        $data['js'][] = 'js/jvectormap/jquery-jvectormap-world-mill-en.js';
        $data['js'][] = 'js/jquery.sparkline.min.js';
        $data['js'][] = 'js/rickshaw/vendor/d3.v3.js';
        $data['js'][] = 'js/rickshaw/rickshaw.min.js';

        View::renderbackendtemplate('header', $data);
        View::render('backend/index', $data);
        View::renderbackendtemplate('footer', $data);
    }
    /**
     * define page title and load template files
     */
    public function urlFormats() {

        // dashboard title
        $data['title'] = $this->lang['dashboard_title'];
        $data['lang'] = $this->lang;

        // active sidebar item
        Session::set( 'active_sidebar_item', 'i-url-formats' );

        // LOAD JS
        $data['js'][] = 'js/jquery.dataTables.min.js';
        $data['js'][] = 'js/dataTables.bootstrap.js';
        $data['js'][] = 'js/select2/select2.min.js';
        $data['js'][] = 'js/jquery.validate.min.js';
        $data['js'][] = 'js/bootstrap-tagsinput.min.js';
        $data['js'][] = 'js/custom/url-formats.js';
        $data['jq'] = 'urlFormats.init();';

        View::renderbackendtemplate('header', $data);
        View::render('backend/settings/url-format', $data);
        View::renderbackendtemplate('footer', $data);
    }

    /**
     * load content from ajax request
     */
    public  function ajaxLoadContent( $file = false )
    {
        View::render( 'backend/ajax/' . $file );
    }

}

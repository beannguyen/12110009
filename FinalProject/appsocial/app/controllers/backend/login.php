<?php namespace controllers\backend;

use core\view;
use helpers\Session;
use helpers\url;

class Login extends \core\controller
{
    private $lang = array();
    // Post vars
    private $user;
    private $pass;
    private $key;

    public $error;

    public $use_emails = false;

    // Misc vars
    private $token;

    /**
     * call the parent construct
     */
    public function __construct()
    {

        parent::__construct();
        $this->_login = new \models\backend\login();

        // init options
        $this->use_emails = $this->generic->getOption('email-as-username-enable');
        $this->username_type = ( $this->use_emails ) ? 'email' : 'username';

        // Call all lang string
        $this->language->load('login');
        $this->lang['_username'] = $this->language->get('_username');
        $this->lang['_email'] = $this->language->get('_email');
        $this->lang['_password'] = $this->language->get('_password');
        $this->lang['_login_in'] = $this->language->get('_login_in');
        $this->lang['_forgot_password'] = $this->language->get('_forgot_password');
        $this->lang['_forgot_password_btn'] = $this->language->get('_forgot_password_btn');
        $this->lang['_remember_me'] = $this->language->get('_remember_me');
    }

    /**
     * Login page
     */
    public function index()
    {
        if ( $this->generic->guestOnly() ) { // Only guest can view this page

            Url::redirect();
        } else {

            $data['use_emails'] = $this->generic->getOption('email-as-username-enable');

            // Redirect the logging in user
            $referer = ( isset( $_SERVER["HTTP_REFERER"] ) ) ? $_SERVER["HTTP_REFERER"] : DIR;
            Session::set( 'referer', $referer );

            // Generate a unique token for security purposes
            $this->generic->generateToken();

            $data['title'] = 'Login';
            $data['lang'] = $this->lang;

            View::renderbackendtemplate('login-header', $data);
            View::render('backend/login/index', $data);
            View::renderbackendtemplate('login-footer', $data);
        }
    }

    /**
     * Login page
     */
    public function forgot()
    {
        if ( $this->generic->guestOnly() ) { // Only guest can view this page

            Url::redirect();
        } else {

            $data['use_emails'] = $this->generic->getOption('email-as-username-enable');

            // Redirect the logging in user
            $referer = ( isset( $_SERVER["HTTP_REFERER"] ) ) ? $_SERVER["HTTP_REFERER"] : DIR;
            Session::set( 'referer', $referer );

            // Generate a unique token for security purposes
            $this->generic->generateToken();

            $data['title'] = 'Login';
            $data['lang'] = $this->lang;

            View::renderbackendtemplate('login-header', $data);
            View::render('backend/login/forgot-password', $data);
            View::renderbackendtemplate('login-footer', $data);
        }
    }

    /**
     * Get userId from email then reset user`s password
     * @param string email email of user
     * @return boolean processing status
     */
    function forgotPassword() {

        if ( isset($_POST['email'] ) ) {

            $email = $this->generic->secure( $_POST['email'] );
            // check for email is exist
            $_register = new \models\backend\register();
            if ( $_register->isExistedEmail( $email ) ) {

                // get userId from email
                $_user = new \models\backend\user();
                $userId = $_user->getUserIdByEmail( $email );
                // random new password
                $newpass = $this->random_password( 8 );
                // change new password
                $change = $_user->changePassword( $userId, \helpers\password::make( $newpass ) );
                if ( $change ) {

                    // if change password processing is success, send an email to user
                    $msg = $this->generic->getOption('email-forgot-msg');
                    $subj = $this->generic->getOption('email-forgot-subj');

                    // get user information
                    $user = $_user->getUser( $userId );
                    // set value for shortcodes
                    $shortcodes = array(
                        'site_address' => DIR,
                        'full_name' => $user['name'],
                        'username' => $user['username'],
                        'reset' => $newpass,
                    );

                    // send an email
                    if (!$this->generic->sendEmail($user['email'], $subj, $msg, $shortcodes)) {
                        echo 'email_not_send';
                        return false;
                    }
                    // all done
                    echo 'changed';
                } else {

                    // failed
                    echo 'not_changed';
                }
            } else {

                echo 'email_not_found';
            }
        } else {

            echo 'error';
        }
        return true;
    }

    /**
     * login process
     */
    public function process()
    {
        // Login form post data
        if(isset($_POST['login'])) :
            $this->user = $this->generic->secure( $_POST[$this->username_type] );
            $this->pass = $this->generic->secure( $_POST['password'] );

            $this->token = !empty( $_POST['token'] ) ? $_POST['token'] : '';
            $processCheck = $this->_login->process($this->user, $this->pass, $this->token);
            if( $processCheck == 'invalid_token' || $processCheck == 'error_found' || $processCheck == 'incorrect_password' || $processCheck == 'banned_user' || $processCheck == 'disable-login' || $processCheck == 'not_active')
            {
                echo $processCheck;
            } else
            {
                echo 'success';
            }
        endif;
    }

    /**
     * Logout from system
     * @param null $flag
     */
    function logout($flag = null)
    {
        /** Check if the browser set a referrer. */
        $redirect = ( isset( $_SERVER["HTTP_REFERER"] ) ) ? $_SERVER["HTTP_REFERER"] : DIR;

        /**
         * Begin removing their existence.
         *
         * Good bye friend :(. Promise you'll come back?!
         */
        $username = \helpers\Session::get('username');
        if ( isset( $username ) ) :
            session_unset();
            session_destroy();
        endif;

        /** Voila! Here we shall gently nudge them somewhere else. */
        if(empty($flag))
        {
            \helpers\url::redirect();
            exit();
        }
    }

    /**
     * Random a password function
     * @param int $length
     * @return string
     */
    private function random_password( $length = 8 ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }

    /*
    // Are they clicking from an email?
    function active($key)
    {
        $this->loadModel('useractive');
        if($key != '') {
            $this->key = $key;
            $key = parent::secure($key);
            $res = $this->model->getKey($key);

            $this->view->error = $res;

        } else {
            $this->view->error = 'link_not_found';
        }
        $this->view->render( 'dashboard/error/msgbox' );
    }

    // get email for resend key
    function resend() {

        if ( isset( $_POST['email-to-resend'] ) ) {
            // get email
            $email = parent::secure( $_POST['email-to-resend'] );
            // check if email is already existed
            $this->loadModel( 'register' );
            $existedUser = $this->model->isExistedEmail( $email );
            if ( $existedUser == 1 ) {
                echo $this->resendKey( $email );
            } else {
                echo 'not_exist_user';
            }
        } else {
            echo 0;
        }
    }

    // Do they want the key resent?
    function resendKey( $email )
    {
        $this->loadModel('useractive');
        echo $this->model->resendKey( $email );
    }
    */
}


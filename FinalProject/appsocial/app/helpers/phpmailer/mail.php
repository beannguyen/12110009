<?php namespace helpers\phpmailer;

class mail extends phpmailer {

    private $generic;

    function __construct()
    {
       $this->generic = new \helpers\generic();
    }
    /**
     * Sends HTML emails with optional shortcodes.
     *
     * @param     string    $to            Receiver of the mail.
     * @param     string    $subj          Subject of the email.
     * @param     string    $msg           Message to be sent.
     * @param     array     $shortcodes    Shortcode values to replace.
     * @param     bool      $bcc           Whether to send the email using Bcc: rather than To:
     *                                     Useful when sending to multiple recepients.
     * @return    bool      Whether the mail was sent or not.
     */
    public function send($to, $subj, $msg, $headers)
    {
        $mail = new PHPMailer();  // tạo một đối tượng mới từ class PHPMailer
        $mail->IsSMTP(); // bật chức năng SMTP
        $mail->IsHTML(true);
        $mail->SMTPDebug = 0;  // kiểm tra lỗi : 1 là  hiển thị lỗi và thông báo cho ta biết, 2 = chỉ thông báo lỗi
        $mail->SMTPAuth = true;  // bật chức năng đăng nhập vào SMTP này
        $mail->SMTPSecure = 'ssl'; // sử dụng giao thức SSL vì gmail bắt buộc dùng cái này
        $mail->Host = $this->generic->getOption('mailserver_url');
        $mail->Port = $this->generic->getOption('mailserver_port');
        $mail->Username = $this->generic->getOption('mailserver_login');
        $mail->Password = $this->generic->getOption('mailserver_pass');
        $mail->addCustomHeader($headers);
        $mail->Subject = "=?UTF-8?B?".base64_encode($subj)."?=";
        $mail->Body = nl2br(html_entity_decode($msg));
        $mail->AddAddress($to);
        $mail->SetFrom($this->generic->getOption('mailserver_login'), $this->generic->getOption('site_name'));
        $mail->AddReplyTo($this->generic->getOption('mailserver_login'), $this->generic->getOption('site_name'));
        if(!$mail->Send()) {
            $error = 'Can not send this email: '.$mail->ErrorInfo;
            return false;
        } else {
            return true;
        }
    }
}
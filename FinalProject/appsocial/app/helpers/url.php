<?php namespace helpers;

/*
 * url Class
 *
 * @author David Carr - dave@daveismyname.com - http://www.daveismyname.com
 * @version 2.0
 * @date June 27, 2014
 */
class Url {

	/**
	 * Redirect to chosen url
	 * @param  string  $url      the url to redirect to
	 * @param  boolean $fullpath if true use only url in redirect instead of using DIR
	 */
	public static function redirect($url = null, $fullpath = false){
		if($fullpath == false){
			$url = DIR . $url;
		}
		
		header('Location: '.$url);
		exit;
	}

	public static function curURL()
    {
        $pageURL = 'http';
        if ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") {
            $pageURL .= "s";
        }
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }

	/**
	 * created the absolute address to the template folder
	 * @return string url to template folder
	 */
	public static function get_template_path(){
		return DIR.'app/templates/'.Session::get('frontend_template').'/';
	}

	public static function get_path(){
		return ROOT.'/app/templates/'.Session::get('frontend_template').'/';
	}

    /**
     * created the absolute address to the template folder
     * @return string url to template folder
     */
    public static function get_backend_template_path(){
        return DIR.'app/templates/'.Session::get('backend_template').'/';
    }

	/**
	 * converts plain text urls into HTML links, second argument will be
	 * used as the url label <a href=''>$custom</a>
	 *
	 * @param  string $text   data containing the text to read
	 * @param  string $custom if provided, this is used for the link label
	 * @return string         returns the data with links created around urls
	 */
	public static function autolink($text, $custom = null) {
		$regex   = '@(http)?(s)?(://)?(([-\w]+\.)+([^\s]+)+[^,.\s])@';

		if ($custom === null) {
			$replace = '<a href="http$2://$4">$1$2$3$4</a>';
		} else {
			$replace = '<a href="http$2://$4">'.$custom.'</a>';
		}
	
		return preg_replace($regex, $replace, $text);
	}

	/**
	 * This function converts and url segment to an safe one, for example:
	 * `test name @132` will be converted to `test-name--123`
	 * Basicly it works by replacing every character that isn't an letter or an number to an dash sign
	 * It will also return all letters in lowercase
	 *
	 * @param $slug - The url slug to convert
	 *
	 * @return mixed|string
	 */
	public static function generateSafeSlug($slug) {

		// transform url
		$slug = preg_replace('/[^a-zA-Z0-9]/', '-', $slug);
		$slug = strtolower(trim($slug, '-'));

		//Removing more than one dashes
		$slug = preg_replace('/\-{2,}/', '-', $slug);

		return $slug;
	}

	/**
	 * Go to the previous url.
	 */
	public static function previous() {

        if ( isset( $_SERVER['HTTP_REFERER'] ) ) {

            header('Location: '. $_SERVER['HTTP_REFERER']);
            exit;
        } else {

            header('Location: '. DIR);
            exit;
        }
	}

    /**
     * Vietnamese permalink creation
     * @param string $title
     * @return string
     */

    public static function vietnamese_permalink($title) {
        /* 	Replace with "-"
            Change it if you want
        */

        $replacement = '-';
        $map = array();
        $quotedReplacement = preg_quote($replacement, '/');

        $default = array(
            '/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ|å/' => 'a',
            '/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ|ë/' => 'e',
            '/ì|í|ị|ỉ|ĩ|Ì|Í|Ị|Ỉ|Ĩ|î/' => 'i',
            '/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ|ø/' => 'o',
            '/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ|ů|û/' => 'u',
            '/ỳ|ý|ỵ|ỷ|ỹ|Ỳ|Ý|Ỵ|Ỷ|Ỹ/'	=> 'y',
            '/đ|Đ/' => 'd',
            '/ç/' => 'c',
            '/ñ/' => 'n',
            '/ä|æ/' => 'ae',
            '/ö/' => 'oe',
            '/ü/' => 'ue',
            '/Ä/' => 'Ae',
            '/Ü/' => 'Ue',
            '/Ö/' => 'Oe',
            '/ß/' => 'ss',
            '/[^\s\p{Ll}\p{Lm}\p{Lo}\p{Lt}\p{Lu}\p{Nd}]/mu' => ' ',
            '/\\s+/' => $replacement,
            sprintf('/^[%s]+|[%s]+$/', $quotedReplacement, $quotedReplacement) => '',
        );
        //Some URL was encode, decode first
        $title = urldecode($title);

        $map = array_merge($map, $default);
        return strtolower( preg_replace(array_keys($map), array_values($map), $title) );
        #---------------------------------o
    }
}

<?php namespace helpers;

use \helpers\database as Database,
    \helpers\session as Session;

class Generic
{
    private $_db;

    function __construct()
    {

        //connect to PDO here.
        $this->_db = Database::get();
        $this->_db->connect();
    }

    /**
     * Retrieves an option value based on option name.
     * @param string $option Name of option to retrieve.
     * @param bool|string $check Whether the option is a checkbox.
     * @param bool|string $profile Whether to return a profile field, or an admin setting.
     * @param bool|int $permission Whether to return a value of user permission
     * @param int|string $id Required if profile is true; the user_id of a user.
     * @return bool|string
     */
    public function getOption($option, $check = false, $profile = false, $permission = false, $id = '')
    {

        if (empty($option)) return false;

        $option = trim($option);

        if ($profile) {
            $sql = "SELECT `meta_value` FROM `" . PREFIX . "user_meta` WHERE `meta_key` = '$option' AND `user_id` = $id LIMIT 1;";
        } elseif ($permission) {
            $sql = "SELECT `permission` FROM `" . PREFIX . "user_permissions` WHERE `action` = '$option' AND `level_id` = $id LIMIT 1;";
        } else {
            $sql = "SELECT option_value FROM " . PREFIX . "options WHERE option_key = '" . $option . "' LIMIT 1;";
        }

        $stmt = $this->_db->query($sql);

        if (!$stmt) return false;

        $result = $this->_db->fetch($stmt);

        if ($result == NULL)
            return false;
        elseif ($profile)
            $res = $result['meta_value'];
        elseif ($permission)
            $res = $result['permission'];
        else
            $res = $result['option_value'];

        if ($check)
            $res = ($res == "1") ? 'checked="checked"' : '';

        return $res;

    }

    /**
     * Updates an option in the database.
     *
     * If an option exists in the database, it will be updated. If it does not exist,
     * the option will be created.
     *
     * @param $option
     * @param $newvalue
     * @param bool $profile
     * @param bool $permission
     * @param string $id
     * @return bool|void
     */
    public function updateOption($option, $newvalue, $profile = false, $permission = false, $id = '')
    {

        $option = trim($option);
        if (empty($option) || !isset($newvalue))
            return false;

        if ($profile)
            $oldvalue = $this->getOption($option, false, true, false, $id);
        elseif ($permission)
            $oldvalue = $this->getOption($option, false, false, true, $id);
        else
            $oldvalue = $this->getOption($option);

        if ($newvalue === $oldvalue)
            return false;

        if (false === $oldvalue) :

            if ($profile) {
                $params = array(
                    'user_id' => $id,
                    'meta_key' => $option,
                    'meta_value' => is_array($newvalue) ? serialize($newvalue) : $newvalue
                );
                $table = PREFIX . 'user_meta';
            } elseif ($permission) {
                $params = array(
                    'level_id' => $id,
                    'action' => $option,
                    'permission' => is_array($newvalue) ? serialize($newvalue) : $newvalue
                );
                $table = PREFIX . 'user_permissions';
            } else {
                $params = array(
                    'option_name' => $option,
                    'option_value' => is_array($newvalue) ? serialize($newvalue) : $newvalue
                );
                $table = PREFIX . 'options';
            }

            return $this->_db->insert($table, $params);
        endif;

        if ($profile) {
            $params = array(
                'meta_value' => is_array($newvalue) ? serialize($newvalue) : $newvalue
            );

            $table = PREFIX . 'user_meta';
            $where = "`meta_key` = '$option' AND `user_id` = $id";
        } elseif ($permission) {
            $params = array(
                'permission' => is_array($newvalue) ? serialize($newvalue) : $newvalue
            );
            $table = PREFIX . 'user_permissions';
            $where = " action = '$option' AND level_id = '$id'";
        } else {
            $params = array(
                'option_value' => is_array($newvalue) ? serialize($newvalue) : $newvalue
            );
            $table = PREFIX . 'options';
            $where = "`option_name` = '$option'";
        }

        return $this->_db->update($table, $params, $where);

    }

    /**
     * Sanitizes titles intended for SQL queries.
     *
     * Specifically, HTML and PHP tag are stripped. The return value
     * is not intended as a human-readable title.
     *
     * @param     string $title The string to be sanitized.
     * @return    string    The sanitized title.
     */
    public function sanitize_title($title)
    {

        $title = strtolower($title);
        $title = preg_replace('/&.+?;/', '', $title); // kill entities
        $title = str_replace('.', '-', $title);
        $title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
        $title = preg_replace('/\s+/', '-', $title);
        $title = preg_replace('|-+|', '-', $title);
        $title = trim($title, '-');

        return $title;

    }

    /**
     * Only allows guests to view page.
     *
     * A logged in user will be shown an error and denied from viewing the page.
     */
    public function guestOnly()
    {

        $username = Session::get('username');
        if (!empty($username)) {
            return true;
        }

        return false;

    }

    /**
     * Generates a unique token.
     *
     * Intended for form validation to prevent exploit attempts.
     */
    public function generateToken()
    {

        $token = Session::get('token');
        if (empty($token))
            Session::set('token', md5(uniqid(mt_rand(), true)));

    }

    /**
     * Prevents invalid form submission attempts.
     *
     * @param     string $token The POST token with a form.
     * @return    bool      Whether the token is valid.
     */
    public function valid_token($token)
    {

        $get_token = Session::get('token');
        if (empty($get_token))
            return false;

        if ($get_token != $token)
            return false;

        return true;

    }

    /**
     * ss any string intended for SQL execution.
     *
     * @param     string $string
     * @return    string    The secured value string.
     */
    public function secure($string)
    {

        // Because some servers still use magic quotes
        if (get_magic_quotes_gpc()) :

            if (!is_array($string)) :
                $string = htmlspecialchars(stripslashes(trim($string)));
            else :
                foreach ($string as $key => $value) :
                    $string[$key] = htmlspecialchars(stripslashes(trim($value)));
                endforeach;
            endif;

            return $string;

        endif;


        if (!is_array($string)) :
            $string = htmlspecialchars(trim($string));
        else :
            foreach ($string as $key => $value) :
                $string[$key] = htmlspecialchars(trim($value));
            endforeach;
        endif;

        return $string;

    }

    /**
     * Finds the current IP address of a visiting user.
     *
     * @return    string    The IP address
     */
    public function getIPAddress()
    {

        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) :
            $ipAddress = $_SERVER["HTTP_X_FORWARDED_FOR"];
        else :
            $ipAddress = isset($_SERVER["HTTP_CLIENT_IP"]) ? $_SERVER["HTTP_CLIENT_IP"] : $_SERVER["REMOTE_ADDR"];
        endif;

        return $ipAddress;
    }

    /**
     * Makes directory and returns BOOL(TRUE) if exists OR made.
     *
     * @param string $path Path name
     * @param int $mode file permission
     * @return bool
     */
    function rmkdir($path, $mode = 0755)
    {
        $path = rtrim(preg_replace(array("/\\\\/", "/\/{2,}/"), "/", $path), "/");
        $e = explode("/", ltrim($path, "/"));
        if (substr($path, 0, 1) == "/") {
            $e[0] = "/" . $e[0];
        }
        $c = count($e);
        $cp = $e[0];
        for ($i = 1; $i < $c; $i++) {
            if (!is_dir($cp) && !@mkdir($cp, $mode)) {
                return false;
            }
            $cp .= "/" . $e[$i];
        }
        return @mkdir($path, $mode);
    }

    /**
     * Split a paragraph
     *
     * @param string $string Paragraph
     * @param $nb_caracs Number of words
     * @param $separator The readmore character
     * @return string
     */
    public static function split_words($string, $nb_caracs, $separator)
    {
        $string = strip_tags(html_entity_decode($string));
        if (strlen($string) <= $nb_caracs) {
            $final_string = $string;
        } else {
            $final_string = "";
            $words = explode(" ", $string);
            foreach ($words as $value) {
                if (strlen($final_string . " " . $value) < $nb_caracs) {
                    if (!empty($final_string)) $final_string .= " ";
                    $final_string .= $value;
                } else {
                    break;
                }
            }
            $final_string .= $separator;
        }
        return $final_string;
    }

    /**
     * Scan directories
     *
     * @param $rootDir
     * @param $allowext
     * @param array $allData
     * @return array
     */
    function scanDirectories($rootDir, $allowext, $allData = array())
    {
        if ( !is_readable( $rootDir ) )
            return false;
        
        $dirContent = scandir($rootDir);

        foreach ($dirContent as $key => $content) {
            $path = $rootDir . '/' . $content;
            $ext = substr($content, strrpos($content, '.') + 1);

            if (in_array($ext, $allowext)) {
                if (is_file($path) && is_readable($path)) {
                    $allData[] = $path;
                } elseif (is_dir($path) && is_readable($path)) {
                    // recursive callback to open new directory
                    $allData = scanDirectories($path, $allData);
                }
            }
        }
        return $allData;
    }

    /**
     * rename image with width and height
     * EX: http://example.com/123.jpg -> http://example.com/123-100x100.jpg
     *
     * @param $url
     * @param $width
     * @param $height
     * @return string
     */
    public function getFileNameWithImageSize($url, $width, $height)
    {
        // get filename
        $urlArray = explode('/', $url);
        $filename = $urlArray[sizeof($urlArray) - 1];
        // get filename and extension
        $file = pathinfo($filename);
        $extension = $file['extension'];
        $filename = $file['filename'];

        // rename to width and height
        $filename .= '-' . $width . 'x' . $height . '.' . $extension;
        // create new array to hold path
        $urlArray[sizeof($urlArray) - 1] = $filename;
        // create new url
        $newURL = '';
        for ($i = 0; $i < sizeof($urlArray); $i++) {
            if ($i == (sizeof($urlArray) - 1)) {
                $newURL .= $urlArray[$i];
                break;
            }
            $newURL .= $urlArray[$i] . '/';
        }
        return $newURL;
    }

    /**
     * Sends HTML emails with optional shortcodes.
     *
     * @param     string    $to            Receiver of the mail.
     * @param     string    $subj          Subject of the email.
     * @param     string    $msg           Message to be sent.
     * @param     array     $shortcodes    Shortcode values to replace.
     * @param     bool      $bcc           Whether to send the email using Bcc: rather than To:
     *                                     Useful when sending to multiple recepients.
     * @return    bool      Whether the mail was sent or not.
     */
    public function sendEmail($to, $subj, $msg, $shortcodes = '') {

        if ( !empty($shortcodes) && is_array($shortcodes) ) :

            foreach ($shortcodes as $code => $value)
                $msg = str_replace('{{'.$code.'}}', $value, $msg);

        endif;

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'From: ' . $this->getOption('admin_email') . "\r\n";
        $headers .= 'Reply-To: ' . $this->getOption('admin_email') . "\r\n";

        $sender = new \helpers\phpmailer\mail();

        return $sender->send($to, $subj, $msg, $headers);
    }
}